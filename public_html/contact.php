<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require('recaptcha-master/src/autoload.php');

// configure
$from = 'gabihomestaging@gmail.com';
$sendTo = 'gabihomestaging@gmail.com';
$subject = 'Message from Contact Form';
$fields = array('name' => 'Name', 'phone' => 'Phone', 'email' => 'Email', 'message' => 'Message');
$okMessage = 'Contact Form successfully submitted.';
$errorMessage = 'There was an error while submitting the form. Please try again later';
$recaptchaSecret = '6Le181cUAAAAAAhpgT6B2_SYXp-342hYRmtb5e7B';
$responseArray = array();

try {
    if ($_POST) {

        if (!isset($_POST['g-recaptcha-response'])) {
            throw new \Exception('ReCaptcha is not set.');
        }

        $recaptcha = new \ReCaptcha\ReCaptcha($recaptchaSecret, new \ReCaptcha\RequestMethod\SocketPost());
        $response = $recaptcha->verify($_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);

        if (!$response->isSuccess()) {
            $error = '';
            foreach ($response->getErrorCodes() as $code) {
                $error .= "$code, ";
            }
            throw new \Exception($error);
        }

        $emailText = "You have new message from contact form\n=============================\n";
        foreach ($_POST as $key => $value) {
            if (isset($fields[$key])) {
                $emailText .= "$fields[$key]: $value\n";
            }
        }

        $headers = array('Content-Type: text/plain; charset="UTF-8";',
            'From: ' . $from,
            'Reply-To: ' . $from,
            'Return-Path: ' . $from,
        );
        mail($sendTo, $subject, $emailText, implode("\n", $headers));
        $responseArray = array('type' => 'success', 'message' => $okMessage);
    }
} catch (\Exception $e) {
    $responseArray = array('type' => 'danger', 'message' => $e->getMessage());
}

if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    $encoded = json_encode($responseArray);

    header('Content-Type: application/json');
    echo $encoded;
} else {
    echo $responseArray['message'];
}
